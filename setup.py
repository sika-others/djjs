#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "djjs",
    version = "0.0.1",
    url = 'https://github.com/sikaondrej/djjs/',
    download_url = 'https://github.com/sikaondrej/djjs/',
    license = 'MIT',
    description = "Django JavaScript utils",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    packages = find_packages(),
    include_package_data = True,
    zip_safe = False,
)
