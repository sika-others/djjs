import json

from django.http import HttpResponse
from django.core.urlresolvers import reverse, NoReverseMatch

def url_view(request):
    data = request.GET.get("data", None)
    if not data:
        return HttpResponse("")
    data = json.loads(data)
    try:
        return HttpResponse(reverse(data["name"], args=data["args"], kwargs=data["kwargs"]))
    except NoReverseMatch:
        return HttpResponse("")
