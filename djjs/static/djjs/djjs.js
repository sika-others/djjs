var djjs = {};

djjs.http = {};

djjs.http.get = function(url){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", url, false);
    xmlhttp.send();
    return xmlhttp.responseText;
}

djjs.url = function(name, args, kwargs) {
    if (args === undefined) args = [];
    if (kwargs === undefined) kwargs = {};
    data = JSON.stringify({name:name, args:args, kwargs:kwargs});
    return djjs.http.get("/djjs/url/?data="+data);
}
