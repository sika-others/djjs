from django.conf.urls import patterns, include, url

from views import url_view


urlpatterns = patterns('',
    url(r'^url/', url_view),
)