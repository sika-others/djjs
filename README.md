djjs
====

Django JavaScript utils

### Authors
*  Ondrej Sika, <http://ondrejsika.com>, ondrej@ondrejsika.com

### Source
* Python Package Index: <http://pypi.python.org/pypi/djjs>
* GitHub: <https://github.com/sikaondrej/djjs>

### Installation

install via pip

    pip install djjs
    
add to installed apps in settings.py

    INSTALLED_APPS += ("djjs", )


and add to html 
	
    <script type="text/javascript" src="{{STATIC_ROOT}}djjs/djjs.js"></script>

## Usage
### Django reverse url
for use `django.core.urlresolvers.reverse` in JavaScript `djjs.url`.

#### Example

Python

	from django.core.urlresolvers import reverse
	reverse("catalog:product:detail", args=["my-favourite-product", ])

JavaScript

    djjs.url("catalog:product:detail", args=["my-favourite-product", ])
